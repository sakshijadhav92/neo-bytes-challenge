# Base image
FROM ubuntu:18.04 

# copy file 
COPY . /data

# execute command
CMD ./data/my_first_script.sh


# Steps to build docker image 

#docker build -t <image name> <path of dockerfile>

# Use image and launch container

#docker run -it --name <container name> <image name>:latest
